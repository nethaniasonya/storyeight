from django.shortcuts import render
from django.http import JsonResponse
import pip._vendor.urllib3 as urllib3

# Create your views here.
def index(request):
    return render(request, 'index.html')

def search(request):
    if request.method == "GET":
        keyword = request.GET.get('keyword', False)
        url = "https://www.googleapis.com/books/v1/volumes?q="
        url += keyword
        data = urllib3.Request(url)
        return JsonResponse(data)