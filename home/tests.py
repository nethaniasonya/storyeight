from django.test import TestCase, Client

# Create your tests here.
class UnitTest(TestCase):

    def test_url_exists(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_page_uses_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')