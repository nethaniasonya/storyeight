$.ajax({
            method: "GET",
            url: "https://www.googleapis.com/books/v1/volumes?q=dog",
            success: function(results){
                for (let i = 0; i < results.items.length; i++) {
                    let element = results.items[i];
                    $("tbody").append(
                        "<tr> <td>" + "<img src='" +
                        element.volumeInfo.imageLinks.thumbnail + "'>" +
                        "</td>" + "<td>" +
                        element.volumeInfo.title + "</td>" +
                        "<td>" +
                        element.volumeInfo.authors +
                        "</td> </tr>"
                    )
                }
            }
        }) 
        $(document).ready(function(){
            $("input").keyup(
                function(){
                    var keyword = $("input").val();
                    var delay = 500;
                    if (keyword.length >= 2) {
                        $.ajax({
                            method: "GET",
                            url: "https://www.googleapis.com/books/v1/volumes?q="+keyword,
                            success: function(results){
                                setTimeout(function(){
                                    $("tbody").empty();
                                    for (let i = 0; i < results.items.length; i++) {
                                        let element = results.items[i];
                                        $("tbody").append(
                                            "<tr> <td>" + "<img src='" +
                                            element.volumeInfo.imageLinks.thumbnail + "'>" +
                                            "</td>" + "<td>" +
                                            element.volumeInfo.title + "</td>" +
                                            "<td>" +
                                            element.volumeInfo.authors +
                                            "</td> </tr>"
                                        )
                                    }   
                                },delay)
                            }
                        })   
                    }
                }
            )
        })